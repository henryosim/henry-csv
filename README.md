# Symfony Docker

A [Docker](https://www.docker.com/)-based installer and runtime for the [Symfony](https://symfony.com) web framework, with full [HTTP/2](https://symfony.com/doc/current/weblink.html), HTTP/3 and HTTPS support.

![CI](https://github.com/dunglas/symfony-docker/workflows/CI/badge.svg)

## Getting Started

1. If not already done, [install Docker Compose](https://docs.docker.com/compose/install/)
2. Run `docker-compose build --pull --no-cache` to build fresh images
3. Run `docker-compose up` (the logs will be displayed in the current shell)
4. Open `https://localhost` in your favorite web browser and [accept the auto-generated TLS certificate](https://stackoverflow.com/a/15076602/1352334)

## Run command
1. Open `http://localhost:8080/?server=database&username=onthemoney&db=onthemoney&select=postcode`in the browser to access database. (use database credentials in .env file to log into databse)  
2. To import data Run ` docker-compose exec php bin/console postcodes:download-import`
3. To run test Run `docker-compose exec php bin/phpunit` 

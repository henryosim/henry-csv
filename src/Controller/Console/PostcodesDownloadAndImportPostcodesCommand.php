<?php


namespace App\Controller\Console;


use App\Entity\Postcode;
use App\Repository\PostcodeRepository;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Filesystem\Exception\FileNotFoundException;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Finder\Finder;
use Symfony\Component\Finder\SplFileInfo;
use Symfony\Component\Serializer\Encoder\CsvEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;

class PostcodesDownloadAndImportPostcodesCommand extends Command
{
    protected static $defaultName = 'postcodes:download-import';
    private $destinationDirectory = './public/postcodes/';
    private $subDirectory = 'Data/multi_csv/';
    private $maxFileSize = '3221223823'; // 3gb;

    /** @var EntityManagerInterface */
    private $entityManager;
    private $numOfNewRecords;
    private $numOfUpdatedRecords;

    public function __construct(EntityManagerInterface $entityManager)
    {
        parent::__construct();
        $this->entityManager = $entityManager;
        $this->numOfNewRecords = 0;
        $this->numOfUpdatedRecords = 0;
    }

    /**
     * Command configure method
     */
    protected function configure()
    {
        $this->setDescription('Download Stock data command')
            ->addArgument(
                'downloadUrl',
                InputArgument::OPTIONAL,
                'enter file download url',
                'https://parlvid.mysociety.org/os/ONSPD/2020-05.zip'
            );
    }

    /**
     * Command execute method
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int
     * @throws Exception
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        ini_set('memory_limit', '-1');
        $remoteAddress = $input->getArgument('downloadUrl');
        $baseName = pathinfo($remoteAddress, PATHINFO_BASENAME);
        $io = new SymfonyStyle($input, $output);

//        $io->writeln(date("Y-m-d h:i:sa") . " - Starting file download..");
//        $this->copyRemoteFile($remoteAddress, $baseName);
//        $io->writeln(date("Y-m-d h:i:sa") . " - Finished file download");
//
//        $io->writeln(date("Y-m-d h:i:sa") . " - Starting data extraction..");
//        $this->extractData($baseName, $this->subDirectory);
//        $io->writeln(date("Y-m-d h:i:sa") . " - Finished data extraction");

        $io->writeln(date("Y-m-d h:i:sa") . " - Starting importing CSVs..");
        $this->processCsvFiles();
        $io->writeln(date("Y-m-d h:i:sa") . " - Finished importing CSVs");

        $io->success('Completed. ' . $this->numOfNewRecords . ' record(s) created and ' . $this->numOfUpdatedRecords . ' record(s) updated');
        return Command::SUCCESS;
    }

    /**
     * Copy file from remote address
     * @param string $remoteAddress
     * @param string $baseName
     * @return void
     * @throws Exception
     */
    private function copyRemoteFile(string $remoteAddress, string $baseName): void
    {
        $filesystem = new Filesystem();
        $filesystem->copy($remoteAddress, $this->destinationDirectory . $baseName);
    }

    /**
     * Open archive and extract data directory
     * @param string $baseName
     * @param string $subDirectory
     * @return void
     * @throws Exception
     */
    private function extractData(string $baseName, string $subDirectory = ''): void
    {
        $filesystem = new Filesystem();
        $file = $this->destinationDirectory . $baseName;
        if (!$filesystem->exists($file)) {
            throw new FileNotFoundException(sprintf('File "%s" could not be found.', $baseName));
        }

        $zip = new \ZipArchive();
        $res = $zip->open($this->destinationDirectory . $baseName);
        if ($res === true) {
            if (!$zip->locateName($subDirectory)) {
                throw new Exception(sprintf('sub directory "%s" could not be found in archive.', $subDirectory));
            }

            $zipEntries = [];
            for ($i = 0; $i < $zip->numFiles; $i++) {
                $stats = $zip->statIndex($i);

                if (!str_starts_with($stats['name'], $subDirectory)) {
                    continue;
                }

                if ($stats['size'] > $this->maxFileSize) {
                    throw new Exception(sprintf('"%s" File too large, decompression denied.', $stats['name']));
                }

                $zipEntries[] = $stats['name'];
            }
            $zip->extractTo($this->destinationDirectory, $zipEntries);
            $zip->close();
        } else {
            throw new Exception(sprintf('Unable to open zip file "%s"', $baseName));
        }
    }

    /**
     * Process csv files, write content to database
     * @throws Exception
     */
    private function processCsvFiles(): void
    {
        $filesystem = new Filesystem();
        $directory = $this->destinationDirectory . $this->subDirectory;
        if (!$filesystem->exists($directory)) {
            throw new Exception(sprintf('could not find directory  "%s"', $directory));
        }

        $finder = new Finder();
        $finder->files()->in($directory);
        foreach ($finder as $file) {
            $this->progressivelyProcessFile($file);
        }
    }

    /**
     * Progressively read csv file in chuncks
     * @param SplFileInfo $file
     * @return void
     */
    private function progressivelyProcessFile(SplFileInfo $file): void
    {
        // Read CSV file
        $handle = fopen($file->getRealPath(), "r");
        $batchSize = 100;
        $lineNumber = 0;
        $header = [];

        // Iterate over every line of the file
        while (($raw_string = fgets($handle)) !== false) {
            if ($lineNumber == 0) {
                $header = str_getcsv($raw_string);
                $lineNumber++;
                continue;
            }

            // Parse the raw csv string with header
            $row = array_combine($header, str_getcsv($raw_string));
            $this->processCsvRow($row);

            if (($lineNumber % $batchSize) == 0) {
                $this->entityManager->flush();
                $this->entityManager->clear();
            }
            $lineNumber++;
        }

        $this->entityManager->flush();
        $this->entityManager->clear();
        fclose($handle);
    }

    /**
     * Save Row to the database
     * @param array $row
     */
    private function processCsvRow(array $row): void
    {
        $row['pcd_min'] = str_replace(' ', '', $row['pcd']);

        /** @var PostcodeRepository $postCodeRepo */
        $postCodeRepo = $this->entityManager->getRepository(Postcode::class);

        /** @var Postcode $existingPostcode */
        $existingPostcode = $postCodeRepo->findOneBy(['postcode' => $row['pcd_min']]);
        if ($existingPostcode instanceof Postcode) {
            $this->updatePostCodeRecord($existingPostcode, $row);
            return;
        }
        $this->createNewPostCodeRecord($row);
    }

    /**
     * Update existing Postcode record
     * @param Postcode $existingPostcode
     * @param array $postcodeItem
     */
    private function updatePostCodeRecord(Postcode $existingPostcode, array $postcodeItem): void
    {
        $existingPostcode->setLatitude($postcodeItem['lat']);
        $existingPostcode->setLongitude($postcodeItem['long']);
        $this->entityManager->persist($existingPostcode);
        $this->numOfUpdatedRecords++;
    }

    /**
     * Create a new postcode record
     * @param array $postcodeItem
     */
    private function createNewPostCodeRecord(array $postcodeItem): void
    {
        $postCode = new Postcode();
        $postCode->setPostcode($postcodeItem['pcd_min']);
        $postCode->setLongitude($postcodeItem['long']);
        $postCode->setLatitude($postcodeItem['lat']);
        $this->entityManager->persist($postCode);
        $this->numOfNewRecords++;
    }
}

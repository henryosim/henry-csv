<?php

namespace App\Controller;

use App\Entity\Postcode;
use App\Repository\PostcodeRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class PostcodeController
 * @package App\Controller
 */
class PostcodeController extends AbstractController
{
    /**
     * @Route(path="/", name="home")
     */
    public function index(): Response
    {
        return $this->json([
            'message' => 'Welcome to Postcode Index!',
            'path' => 'src/Controller/PostcodeController.php',
            'routes' => [
                'postcode-search/{partial}' => 'return postcodes with partial string matches',
                'postcode-location/{latitude}/{longitude}' => 'return postcodes near a specified location (latitude / longitude)',
            ]
        ]);
    }

    /**
     * @Route(path="postcode-search/{partial}", name="postcode-search")
     */
    public function search(string $partial, EntityManagerInterface $entityManager): Response
    {
        /** @var PostcodeRepository $postcodeRepo */
        $postcodeRepo = $entityManager->getRepository(Postcode::class);
        $postcodes = $postcodeRepo->findByPartialPostcodeMatch($partial);
        return $this->json([
            'postcodes' => $postcodes,
        ]);
    }

    /**
     * @Route(path="postcode-location/{latitude}/{longitude}", name="postcode-ocation")
     */
    public function location(float $latitude, float $longitude, EntityManagerInterface $entityManager): Response
    {
        /** @var PostcodeRepository $postcodeRepo */
        $postcodeRepo = $entityManager->getRepository(Postcode::class);
        $postcodes = $postcodeRepo->findBy(['latitude' => $latitude, 'longitude' => $longitude]);
        return $this->json([
            'postcodes' => $postcodes,
        ]);
    }
}

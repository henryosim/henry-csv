<?php


namespace App\Tests;


use App\Entity\Postcode;
use App\Repository\PostcodeRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class PostcodeTest extends  KernelTestCase
{
    /** @var EntityManagerInterface */
    private $entityManager;

    protected function setUp(): void
    {
        $kernel = self::bootKernel();
        DatabasePrimer::prime($kernel);

        $this->entityManager = $kernel->getContainer()->get('doctrine')->getManager();
    }

    protected function tearDown(): void
    {
        parent::tearDown();

        $this->entityManager->close();
        $this->entityManager = null;
    }

    /** @test  */
    public function a_postcode_record_can_be_created_in_the_database(){
        $postcode = new Postcode();
        $postcodeString = 'LU61FY';
        $lat = 11.101474;
        $long = -2.242851;
        $postcode->setPostcode($postcodeString);
        $postcode->setLatitude($lat);
        $postcode->setLongitude($long);

        $this->entityManager->persist($postcode);
        $this->entityManager->flush();

        /** @var PostcodeRepository $postcodeRepo */
        $postcodeRepo = $this->entityManager->getRepository(Postcode::class);

        /** @var Postcode $postcodeEntity */
        $postcodeEntity = $postcodeRepo->findOneBy(['postcode'=>'LU61FY']);
        $this->assertEquals($postcodeString, $postcodeEntity->getPostcode());
        $this->assertEquals($lat, $postcodeEntity->getLatitude());
        $this->assertEquals($long, $postcodeEntity->getLongitude());

        $this->assertIsString($postcodeEntity->getPostcode());
        $this->assertIsFloat($postcodeEntity->getLatitude());
        $this->assertIsFloat($postcodeEntity->getLongitude());
    }
}
